package ch.bfh.stahlos.tinkerforge.mqttclient;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.tinkerforge.BrickletLCD20x4;
import com.tinkerforge.IPConnection;

/**
 * This class handle the mqtt for the tinker forge from STAHLOS
 * 
 * implements the MqttCallback from eclipse.paho
 * @author Kappadona
 *
 */
public class MqttLcdClient implements MqttCallback{
	//Tinker Forge red brick address
	private static final String HOST = "localhost";
	private static final int PORT = 4223;
	private static final String UID = "qHo";
	
	//we need an instance of MqttClient from eclipse.paho
	private MqttClient client;
	//Define a standard char set
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	//Give a topic name for publishing messages
	private String topicName = "TinkerForge";
	
	//Mqtt client name, with this name you will be known on the broker side
	private String clientName = "Tinker Forge LCD Client";
	
	 //setup brokerIp and brokerPort String to get filled
	private String brokerIp = "10.0.233.19";
	private String brokerPort = "1883";
	
	public MqttLcdClient(){
		doConnection();
	}
	/**
	 * This method is used to open a connection with the broker
	 */
	public void doConnection(){		
		//now we are ready to try to connecting with the broker
		try {
			//configure the MqttClient with the ip, the port and giving a client name
			client = new MqttClient(
					"tcp://" + brokerIp + ":" + brokerPort, 
					clientName,
					new MemoryPersistence()
					);
			//connect
	        client.connect();
	        
	        //subscribe the topic
	        client.subscribe(topicName);
	        
			System.out.println("connected");
			
		} catch (MqttException e) {
			e.printStackTrace();
			//in case of an exception while connecting, we try again
			doConnection();
		}
	}

	/**
	 * will be called if the connection is lost
	 */
	@Override
	public void connectionLost(Throwable arg0) {
		//Connection lost, we try to connect again! This happen automatically
		System.out.println("Connection lost");
		System.out.println("Try to reconnect!");
	}

	//We don't need this methods from the interface
	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {}
	
	
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		IPConnection ipcon = new IPConnection(); // Create IP connection
		BrickletLCD20x4 lcd = new BrickletLCD20x4(UID, ipcon); // Create device object

		ipcon.connect(HOST, PORT); // Connect to brickd
		// Don't use device before ipcon is connected

		// Turn backlight on
		lcd.backlightOn();
		// Clear Display
		lcd.clearDisplay();
		//split the message on the regex ","
		String[] text = message.toString().split(",");
		try{
			lcd.writeLine((short)0, (short)1, text[0]);
			lcd.writeLine((short)1, (short)1, text[1]);
			lcd.writeLine((short)2, (short)1, text[2]);
			lcd.writeLine((short)3, (short)1, text[3]);
		}catch(IndexOutOfBoundsException e){
			//Ends here if we dont need 4 lines of text
		}
		

		ipcon.disconnect();
	}
	
	
	//open the constructor of the Client to start the service
	public static void main(String[] args) {
		new MqttLcdClient();
	}
}
